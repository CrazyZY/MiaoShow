//
//  LoginViewController.m
//  MiaoShow
//
//  Created by zhangyang on 2017/5/26.
//  Copyright © 2017年 zhangyang. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
    
    @property(nonatomic,strong) IJKFFMoviePlayerController *player;
    
    @end

@implementation LoginViewController
    
#pragma mark - life cycle
    - (void)viewWillDisappear:(BOOL)animated{
        [super viewWillDisappear:animated];
        [self.player shutdown];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    - (void)viewDidDisappear:(BOOL)animated{
        [super viewDidDisappear:animated];
        [self.player.view removeFromSuperview];
        self.player = nil;
    }
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.player.view];

    // 监听视频是否播放完成
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinish) name:IJKMPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stateDidChange) name:IJKMPMoviePlayerLoadStateDidChangeNotification object:nil];
    
     [self.player play];
}
    
    - (void)stateDidChange
    {
        if ((self.player.loadState & IJKMPMovieLoadStatePlaythroughOK) != 0) {
            if (!self.player.isPlaying) {
                [self.player play];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                });
            }
        }
    }
    
- (void)didFinish
    {
        // 播放完之后, 继续重播
        [self.player play];
    }
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
#pragma mark - getter and setter
    
    - (IJKFFMoviePlayerController *)player{
        if (!_player) {
            //随机播放一组视频
            NSString *path = arc4random_uniform(10) % 2 ? @"login_video" : @"loginmovie";
            NSLog(@"%@", path);

            _player = [[IJKFFMoviePlayerController alloc] initWithContentURLString:@"loginmovie" withOptions:[IJKFFOptions optionsByDefault]];
            
            NSLog(@"path === =%@", [[NSBundle mainBundle] pathForResource:path ofType:@"mp4"]);
            // 设计player
            _player.view.frame = self.view.bounds;
            
            _player.scalingMode = IJKMPMovieScalingModeAspectFill;
            // 设置自动播放
            _player.shouldAutoplay = NO;
            // 准备播放
            [_player prepareToPlay];
        }
        return _player;
    }
    
    
    
    
    @end
