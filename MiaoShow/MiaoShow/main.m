//
//  main.m
//  MiaoShow
//
//  Created by zhangyang on 2017/5/26.
//  Copyright © 2017年 zhangyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
