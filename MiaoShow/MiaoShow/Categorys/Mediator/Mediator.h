//
//  Mediator.h
//  URLRoute
//
//  Created by zhangyang on 2017/5/17.
//  Copyright © 2017年 zhangyang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
  1.可以把所有的界面跳转都集中起来，注册到router，固定的界面跳转可以在本地创建plist文件，创建固定的 key／value，跳转时 open：固定的key
  2.变动的界面跳转可以根据后台返回的接口字段绑定
  3.Router 的本质是一个约定、一种传参规则。
 */
@interface Mediator : NSObject

+ (instancetype)shareManager;
- (void)regsisterAllRouter;

@end
