//
//  Mediator.m
//  URLRoute
//
//  Created by zhangyang on 2017/5/17.
//  Copyright © 2017年 zhangyang. All rights reserved.
//

#import "Mediator.h"

@implementation Mediator

+ (instancetype)shareManager{
    static Mediator *_mediator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mediator = [[Mediator alloc]init];
    });
    return _mediator;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)regsisterAllRouter{
//    //首先需要注册url，urlroute只能识别注册过的url，在注册url的同时也注册参数信息
//    [[Routable sharedRouter] map:@"users/:id/:name/:model" toController:[FirstViewController class]];
//    [[Routable sharedRouter] map:@"second" toController:[SecondViewController class]];
//    [[Routable sharedRouter] map:@"third" toController:[ThirdViewController class]];
}

@end
