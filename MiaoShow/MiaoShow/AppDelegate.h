//
//  AppDelegate.h
//  MiaoShow
//
//  Created by zhangyang on 2017/5/26.
//  Copyright © 2017年 zhangyang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

